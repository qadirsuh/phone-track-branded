package net.eneiluj.nextcloud.phonetrack.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class UtilsPerf {


    public static final String PREFS_NAME = "AUTOCREATED_JOB_PREF";
    public static final String KEY_AUTO_SAVED = "PREFS_AUTO_SAVED";

    public static void saveSharedPrefBoolean(Context mContext, boolean value) {

        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(KEY_AUTO_SAVED, value);
        editor.commit();
    }


    public static boolean getSharedPrefBoolean(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        try {
            boolean pref = prefs.getBoolean(KEY_AUTO_SAVED, false);
            Log.e("get pref", pref + "|");
            return pref;
        } catch (Exception e) {
            Log.e("ExceptionIn", e.getMessage());
            Log.e("ExceptionInCase", e.getCause().getMessage());
            e.printStackTrace();
        }

        return false;
    }
}

